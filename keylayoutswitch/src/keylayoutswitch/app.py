"""
Keyboard layout switcher for Elive
"""
import toga
from toga.style import Pack
from toga.style.pack import COLUMN, ROW
import subprocess

#constants
checklayoutCommand = "setxkbmap -query | awk '/layout/ {print $2}'"

def _(string):
    return string #I don't want to do gettext yet but I don't want to convert every statement in the future so let's do it now


class EliveKeyboardLayoutSwitcher(toga.App):

    def startup(self):
        """
        Construct and show the Toga application.

        Usually, you would add your application to a main content box.
        We then create a main window (with a name matching the app), and
        show the main window.
        """
        main_box = toga.Box()

        layouts = EliveKeyboardLayoutSwitcher.getListOfLayouts() #more of a tuple than a list, hmmm? :P
        currentLayout = EliveKeyboardLayoutSwitcher.getKeyboardLayout()

        layoutSelection = toga.Selection(items=list(layouts))
        main_box.add(toga.Label(_("Keyboard Layout: ")))
        print(f"Possibly Selected Variants: {layouts[currentLayout]}")
        main_box.add(layoutSelection)

        self.main_window = toga.MainWindow(title=_(self.formal_name))
        self.main_window.content = main_box
        self.main_window.show()
    def getListOfLayouts(): #https://stackoverflow.com/a/1271932/9654083
        LAYOUTS = []
        VARIANTS = []
        BOTH = []
        BOTH_v2 = {}
        import lxml.etree
        tree = lxml.etree.parse("/usr/share/X11/xkb/rules/base.xml")
        layouts = tree.xpath("//layout")
        for layout in layouts:
            layoutName = layout.xpath("./configItem/name")[0].text
            for variant in layout.xpath("./variantList/variant/configItem/name"):
                variantName = variant.text
                BOTH.append(_("%s, variant %s") % (layoutName, variantName))
                BOTH = list(dict.fromkeys(BOTH))
        for layout in layouts:
            layoutName = layout.xpath("./configItem/name")[0].text
            variantList = []
            for variant in layout.xpath("./variantList/variant/configItem/name"):
                variantName = variant.text
                variantList.append(variantName)
            BOTH_v2[layoutName] = variantList
        print(BOTH_v2)
        return BOTH_v2
    def getKeyboardLayout():
        command = subprocess.Popen(checklayoutCommand, shell=True, stdout=subprocess.PIPE) #not sure if i need shell=True, but it won't hurt anything in this usage, other than linting programs :P
        currentLayout = command.communicate()[0].decode().replace("\n","")
        print(f"Our current-layout is {currentLayout}") #f-strings are MUCH better than %s-strings :P, but they don't work in python < 3.6
        return currentLayout


def main():
    return EliveKeyboardLayoutSwitcher()
